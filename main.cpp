#include "src/BinaryTree.h"
#include <iostream>
#include <memory>

int main() {
    std::string input = "";
    std::getline(std::cin, input);

    auto tree { std::make_unique<BinaryTree<float>>(input) };
    tree->display();

    return 0;
}
