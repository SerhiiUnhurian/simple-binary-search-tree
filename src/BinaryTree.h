#ifndef BINARY_TREE_BINARYTREE_H
#define BINARY_TREE_BINARYTREE_H

#include "Node.h"
#include <cstring>
#include <sstream>
#include <memory>
#include <iostream>

template <class T>
class BinaryTree {
private:
    std::unique_ptr<Node<T>> _root {nullptr};

    void add(T data, std::unique_ptr<Node<T>>& node) {

        if (!node) {
            node = std::make_unique<Node<T>>(data);
        }
        else if (data == node->_data) {
            return;
        }
        else if (data < node->_data) {
            add(data, node->_left);
        }
        else if (data > node->_data) {
            add(data, node->_right);
        }
    }

    bool check(T data, std::unique_ptr<Node<T>>& node) {
        if (!node) {
            return false;
        }
        else if (data == node->_data) {
            return true;
        }
        else if (data < node->_data) {
            return check(data, node->_left);
        }
        else if (data > node->_data) {
            return check(data, node->_right);
        }
    }

    void inorder(std::unique_ptr<Node<T>>& node) {
        if (!node) {
            return;
        }
        inorder(node->_left);
        std::cout << node->_data << " ";
        inorder(node->_right);
    }

    void parseStr(const std::string &input) {
        std::stringstream ss;
        ss << input;

        std::string tmp;
        T data;

        while (!ss.eof()) {
            ss >> tmp;
            if (std::stringstream(tmp) >> data) {
                if (data == 0)
                    break;
                add(data);
            }
        }
    }

public:
    BinaryTree() = default;

    BinaryTree(std::string& input) : BinaryTree() {
        parseStr(input);
    }

    void add(T data) {
        add(data, _root);
    }

    bool check(T data) {
        return  check(data, _root);
    }

    void display() {
        inorder(_root);
    }
};

#endif //BINARY_TREE_BINARYTREE_H
