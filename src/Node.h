#ifndef BINARY_TREE_NODE_H
#define BINARY_TREE_NODE_H

#include <memory>

template <class T>
class Node {
public:
    T _data;
    std::unique_ptr<Node> _left;
    std::unique_ptr<Node> _right;

    Node(T data) : _data(data), _left(nullptr), _right(nullptr) {}
};

#endif //BINARY_TREE_NODE_H
